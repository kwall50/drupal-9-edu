<?php

namespace Drupal\custom_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Custom' Block.
 *
 * @Block(
 *   id = "events_switch_block",
 *   admin_label = @Translation("Events Switch Block"),
 *   category = @Translation("SITE Events Switch Block"),
 * )
 */
class EventsSwitchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // See events-switch-block.html.twig.
    $build['content'] = [
      '#theme' => 'events_switch_block',
    ];

    return $build;
  }

}
